import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import myData from '../../../../data.json'

const initialState = {
    loading: false,
    value: [], // empty array
    error: ""
}

export const getProducts = () => myData;


export const productsSlice = createSlice({
    name: "products",
    initialState,
    reducers: {
        setProducts(state, action) {
            state.value.push(...getProducts().products)
        }
    },
});

export const { setProducts } = productsSlice.actions

export default productsSlice.reducer;